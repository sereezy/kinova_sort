
"use strict";

let FingerPosition = require('./FingerPosition.js');
let KinovaPose = require('./KinovaPose.js');
let JointAngles = require('./JointAngles.js');
let CartesianForce = require('./CartesianForce.js');
let JointVelocity = require('./JointVelocity.js');
let JointTorque = require('./JointTorque.js');
let PoseVelocity = require('./PoseVelocity.js');
let SetFingersPositionGoal = require('./SetFingersPositionGoal.js');
let SetFingersPositionResult = require('./SetFingersPositionResult.js');
let Arm_KinovaPoseGoal = require('./Arm_KinovaPoseGoal.js');
let ArmPoseGoal = require('./ArmPoseGoal.js');
let Arm_KinovaPoseActionResult = require('./Arm_KinovaPoseActionResult.js');
let ArmPoseActionFeedback = require('./ArmPoseActionFeedback.js');
let ArmJointAnglesAction = require('./ArmJointAnglesAction.js');
let SetFingersPositionActionGoal = require('./SetFingersPositionActionGoal.js');
let ArmJointAnglesResult = require('./ArmJointAnglesResult.js');
let ArmPoseResult = require('./ArmPoseResult.js');
let ArmPoseActionResult = require('./ArmPoseActionResult.js');
let ArmPoseAction = require('./ArmPoseAction.js');
let Arm_KinovaPoseAction = require('./Arm_KinovaPoseAction.js');
let SetFingersPositionAction = require('./SetFingersPositionAction.js');
let Arm_KinovaPoseActionGoal = require('./Arm_KinovaPoseActionGoal.js');
let SetFingersPositionFeedback = require('./SetFingersPositionFeedback.js');
let ArmJointAnglesFeedback = require('./ArmJointAnglesFeedback.js');
let Arm_KinovaPoseFeedback = require('./Arm_KinovaPoseFeedback.js');
let Arm_KinovaPoseActionFeedback = require('./Arm_KinovaPoseActionFeedback.js');
let ArmJointAnglesActionGoal = require('./ArmJointAnglesActionGoal.js');
let ArmJointAnglesGoal = require('./ArmJointAnglesGoal.js');
let ArmJointAnglesActionResult = require('./ArmJointAnglesActionResult.js');
let ArmPoseFeedback = require('./ArmPoseFeedback.js');
let SetFingersPositionActionFeedback = require('./SetFingersPositionActionFeedback.js');
let ArmJointAnglesActionFeedback = require('./ArmJointAnglesActionFeedback.js');
let SetFingersPositionActionResult = require('./SetFingersPositionActionResult.js');
let ArmPoseActionGoal = require('./ArmPoseActionGoal.js');
let Arm_KinovaPoseResult = require('./Arm_KinovaPoseResult.js');

module.exports = {
  FingerPosition: FingerPosition,
  KinovaPose: KinovaPose,
  JointAngles: JointAngles,
  CartesianForce: CartesianForce,
  JointVelocity: JointVelocity,
  JointTorque: JointTorque,
  PoseVelocity: PoseVelocity,
  SetFingersPositionGoal: SetFingersPositionGoal,
  SetFingersPositionResult: SetFingersPositionResult,
  Arm_KinovaPoseGoal: Arm_KinovaPoseGoal,
  ArmPoseGoal: ArmPoseGoal,
  Arm_KinovaPoseActionResult: Arm_KinovaPoseActionResult,
  ArmPoseActionFeedback: ArmPoseActionFeedback,
  ArmJointAnglesAction: ArmJointAnglesAction,
  SetFingersPositionActionGoal: SetFingersPositionActionGoal,
  ArmJointAnglesResult: ArmJointAnglesResult,
  ArmPoseResult: ArmPoseResult,
  ArmPoseActionResult: ArmPoseActionResult,
  ArmPoseAction: ArmPoseAction,
  Arm_KinovaPoseAction: Arm_KinovaPoseAction,
  SetFingersPositionAction: SetFingersPositionAction,
  Arm_KinovaPoseActionGoal: Arm_KinovaPoseActionGoal,
  SetFingersPositionFeedback: SetFingersPositionFeedback,
  ArmJointAnglesFeedback: ArmJointAnglesFeedback,
  Arm_KinovaPoseFeedback: Arm_KinovaPoseFeedback,
  Arm_KinovaPoseActionFeedback: Arm_KinovaPoseActionFeedback,
  ArmJointAnglesActionGoal: ArmJointAnglesActionGoal,
  ArmJointAnglesGoal: ArmJointAnglesGoal,
  ArmJointAnglesActionResult: ArmJointAnglesActionResult,
  ArmPoseFeedback: ArmPoseFeedback,
  SetFingersPositionActionFeedback: SetFingersPositionActionFeedback,
  ArmJointAnglesActionFeedback: ArmJointAnglesActionFeedback,
  SetFingersPositionActionResult: SetFingersPositionActionResult,
  ArmPoseActionGoal: ArmPoseActionGoal,
  Arm_KinovaPoseResult: Arm_KinovaPoseResult,
};
