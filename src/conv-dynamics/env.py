#! /usr/bin/env python
from __future__ import print_function
from gym import spaces

import rospy
import tf
import math

from threading import Thread
import numpy as np
import cv_bridge
import cv2
from kinova_msgs.msg import PoseVelocity
from kinova_msgs.msg import JointAngles
from kinova_msgs.msg import JointVelocity

import kinova_msgs.srv
from sensor_msgs.msg import CompressedImage
# from sensor_msgs.msg import Image
import geometry_msgs.msg

from helpers.position_action_client import position_client, move_to_position, move_to_joint_position



class RobotReacher:
    def __init__(self):
        # Images - 84,84
        self.observation_space = spaces.Box(0, 1, (84,84,3))
        # 6DOF Kinova (No Gripper)
        self.action_space      = spaces.Box(0, 1, (6,))

        # self.HOME = ([0.21045614779,-0.260988265276,0.476835846901], 
        #              [0.582824289799,0.396980941296,0.370518654585,0.604514658451])
        self.HOME = [274.8, 174.98, 78.7, 243.27, 83.93, 75.34, 0.0]

        rospy.init_node('Reacher')

        self.image_sub = rospy.Subscriber('/usb_cam/image_raw/compressed',
                                          CompressedImage,
                                          self._get_image,
                                          queue_size=1)
        self.pose_sub  = rospy.Subscriber('/m1n6s200_driver/out/tool_pose',
                                          geometry_msgs.msg.PoseStamped,
                                          self._get_pose,
                                          queue_size=1)
        self.joint_sub = rospy.Subscriber('/m1n6s200_driver/out/joint_angles',
                                          JointAngles, 
                                          self._get_joints,
                                          queue_size=1)
        self.vel_pub   = rospy.Publisher('/m1n6s200_driver/in/joint_velocity',
                                         JointVelocity,
                                         queue_size=1)

        self.tf_listener = tf.TransformListener()
        

        self.cv_bridge = cv_bridge.CvBridge()

        self.joint_vel = [0, 0, 0, 0, 0, 0, 0]
        self.target    = [0.33, -0.20, 0.67]
        rospy.wait_for_message("/usb_cam/image_raw/compressed", CompressedImage)
        rospy.sleep(1)

        # Start thread that continuously sends joint velocities
        self.thread = Thread(target = self._run_thread)
        self.thread.start()

        self.reset()


    def reset(self):
        self._go_to_joint_pos(self.HOME)
        return self._observe()

    def step(self, action):
        velocities = np.clip(np.array(action), -1, 1)
        velocities = 20*velocities
        self.joint_vel = velocities.tolist()

        print("STEPPING")
        done = self._done()
        print('DONE:', done)
        return self._observe(), self._reward(), done

    def render(self):
        cv2.imshow("Img", self.image)
        cv2.waitKey(1)

    def _cost(self):
        return self._sum_squared_dist(self.target)

    def _reward(self):
        return -self._cost()

    def _observe(self):
        return self.image

    def _done(self):
        out_of_bounds = False
        for i in range(self.action_space.shape[0]):
            (trans, rot) = self.tf_listener.lookupTransform('/root', '/m1n6s200_link_{}'.format(i+1), rospy.Time(0))
            if trans[2] < 0.1:
                out_of_bounds = True

        return self._cost() < 0.01 or out_of_bounds or self._check_self_collision()

    def _get_pose(self, msg):
        self.pose = msg.pose
        # if self.pose.position.z < 0.1:
        #     self.reset()

    def _get_joints(self, msg):
        self.joints = msg

    def _get_image(self, msg):
        #TODO Sync Image Timestamps
        # image = self.cv_bridge.imgmsg_to_cv2(msg, 'bgr8')
        np_array = np.fromstring(msg.data, np.uint8)
        image = cv2.imdecode(np_array, cv2.IMREAD_COLOR)
        self.image = image[15:,125:375,:]

    def _run_thread(self):
        try:
            r = rospy.Rate(100)
            while not rospy.is_shutdown():
                self.vel_pub.publish(JointVelocity(*self.joint_vel))
                r.sleep()
        except KeyboardInterrupt:
            print("FUCK")

    def _sum_squared_dist(self, target):
        dist = (self.pose.position.x - target[0])**2 + \
               (self.pose.position.y - target[1])**2 + \
               (self.pose.position.z - target[2])**2
        return dist

    def _joint_distances(self, pos):
        dist = [pos[i] - eval('self.joints.joint{}'.format(i+1)) for i in range(len(pos))]
        return dist

    def _go_to_joint_pos(self, pos):
        while np.linalg.norm(np.array(self._joint_distances(self.HOME))) > 1:
            self.joint_vel = self._joint_distances(pos)
        print("ARRIVED")
        self.joint_vel = [0]*(self.action_space.shape[0] + 1)
    
    def _check_self_collision(self):
        for i in range(self.action_space.shape[0]):
            for j in range(self.action_space.shape[0]):
                if i != j:
                    if (i != 3 and j != 4) and (i !=4 and j!=3):
                        (trans, rot) = self.tf_listener.lookupTransform('/m1n6s200_link_{}'.format(j+1), '/m1n6s200_link_{}'.format(i+1), rospy.Time(0))
                        if np.linalg.norm(trans) < 0.10:
                            return True
        return False









env = RobotReacher()
EPISODES = 10

for ep in range(EPISODES):    
    obs = env.reset()
    done = False
    print("Done:", done)
    while not done:
        action = np.random.randn(7)
        action[1] += 20
        action[4] += 20
        action[5] += 20
        # action = [0]*(env.action_space.shape[0] + 1)
        # print("Action is:", action)
        obs, rew, done = env.step(action)
        env.render()
    env.joint_vel = [0]*(env.action_space.shape[0] + 1)
    print("Completed Episode " + str(ep))


print("FINISHED")

