# CMake generated Testfile for 
# Source directory: /home/guest/serena-ws/kinova_sort/src
# Build directory: /home/guest/serena-ws/kinova_sort/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("kinova-ros/kinova_bringup")
subdirs("kinova-ros/kinova_control")
subdirs("kinova-ros/kinova_gazebo")
subdirs("affordance_rl/ggcnn_kinova_grasping/ggcnn_kinova_grasping")
subdirs("kinova-ros/kinova_msgs")
subdirs("darknet_ros/darknet_ros_msgs")
subdirs("darknet_ros/darknet_ros")
subdirs("kinova-ros/kinova_driver")
subdirs("kinova-ros/kinova_demo")
subdirs("realsense/realsense2_camera")
subdirs("kinova-ros/kinova_moveit/inverse_kinematics_plugins/ikfast/j2n6s300_ikfast")
subdirs("kinova-ros/kinova_moveit/inverse_kinematics_plugins/ikfast/j2s7s300_ikfast")
subdirs("kinova-ros/kinova_moveit/inverse_kinematics_plugins/ikfast/m1n6s300_ikfast")
subdirs("kinova-ros/kinova_moveit/kinova_arm_moveit_demo")
subdirs("kinova-ros/kinova_description")
subdirs("kinova-ros/kinova_moveit/robot_configs/j2n6s300_moveit_config")
subdirs("kinova-ros/kinova_moveit/robot_configs/j2s7s300_moveit_config")
subdirs("kinova-ros/kinova_moveit/robot_configs/m1n6s300_moveit_config")
